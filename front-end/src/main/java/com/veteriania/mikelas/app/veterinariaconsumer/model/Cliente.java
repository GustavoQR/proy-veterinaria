package com.veteriania.mikelas.app.veterinariaconsumer.model;

import java.util.Set;

public class Cliente {

    private Long id;
    private String nombre;
    private String apellido;
    private String dni;
    private String telefono;
    private Set<Mascota> mascotas;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getDni() {
        return dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public Set<Mascota> getMascotas() {
        return mascotas;
    }
    public void setMascotas(Set<Mascota> mascotas) {
        this.mascotas = mascotas;
    }
}
