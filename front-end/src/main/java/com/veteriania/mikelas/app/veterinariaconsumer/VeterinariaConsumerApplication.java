package com.veteriania.mikelas.app.veterinariaconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeterinariaConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VeterinariaConsumerApplication.class, args);
	}

}
