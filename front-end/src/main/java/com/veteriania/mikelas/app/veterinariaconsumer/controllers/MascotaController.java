package com.veteriania.mikelas.app.veterinariaconsumer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class MascotaController {

    @Autowired
    RestTemplate restTemplate;
    @Value("${url..general.api}")
    private String url;

    @GetMapping({"","/","/mascota"})
    public String mascota(Model model){
        List clientes = restTemplate.getForObject(url+"api/cliente", List.class);
        model.addAttribute("titulo","Listado de Clientes");
        model.addAttribute("clientes",clientes);
        return "/mascota/listarMascota";
    }

}
