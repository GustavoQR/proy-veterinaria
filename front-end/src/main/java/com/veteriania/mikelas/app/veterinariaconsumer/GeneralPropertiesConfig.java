package com.veteriania.mikelas.app.veterinariaconsumer;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource("classpath:/properties/general.properties")
})
public class GeneralPropertiesConfig {


}
