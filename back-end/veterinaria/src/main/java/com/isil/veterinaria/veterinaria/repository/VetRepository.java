package com.isil.veterinaria.veterinaria.repository;

import com.isil.veterinaria.veterinaria.model.Vet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VetRepository extends JpaRepository <Vet, Long>{
}
