package com.isil.veterinaria.veterinaria.resource;

import com.isil.veterinaria.veterinaria.model.Cliente;
import com.isil.veterinaria.veterinaria.model.Mascota;
import com.isil.veterinaria.veterinaria.service.MascotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/mascota")
public class MascotaResource {

    @Autowired
    private MascotaService mascotaService;

    @PostMapping
    public ResponseEntity create(@RequestBody Mascota mascota){

        mascotaService.create(mascota);

        return new ResponseEntity(mascota, HttpStatus.CREATED);
    }
}
