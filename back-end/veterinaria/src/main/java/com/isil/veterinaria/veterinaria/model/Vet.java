package com.isil.veterinaria.veterinaria.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "vet")
public class Vet {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_vet")
    private Long id;
    @Column(name = "vName")
    private String vName;
    @Column(name = "lastName")
    private String lastName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvName() {
        return vName;
    }

    public void setvName(String vName) {
        this.vName = vName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
