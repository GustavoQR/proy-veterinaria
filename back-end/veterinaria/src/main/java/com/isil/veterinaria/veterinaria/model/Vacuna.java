package com.isil.veterinaria.veterinaria.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "vacuna")
public class Vacuna {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_vacuna")
    private Long id;
    @Column(name = "nom_vacuna")
    private String nom_vacuna;

    @Column(name = "id_mascota")
    private Long mascota_id;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom_vacuna() {
        return nom_vacuna;
    }

    public void setNom_vacuna(String nom_vacuna) {
        this.nom_vacuna = nom_vacuna;
    }

    public Long getMascota_id() {
        return mascota_id;
    }

    public void setMascota_id(Long mascota_id) {
        this.mascota_id = mascota_id;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fecha")
    private LocalDate fecha = LocalDate.parse("2018-11-02");

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fecha_proxima")
    private LocalDate fecha_proxima = fecha.plusDays(180);
}
