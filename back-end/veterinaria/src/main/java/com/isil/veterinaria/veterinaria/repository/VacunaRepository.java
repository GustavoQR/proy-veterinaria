package com.isil.veterinaria.veterinaria.repository;

import com.isil.veterinaria.veterinaria.model.Vacuna;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacunaRepository extends JpaRepository<Vacuna, Long> {
}
