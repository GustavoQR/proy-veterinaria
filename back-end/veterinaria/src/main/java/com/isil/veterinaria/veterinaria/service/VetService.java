package com.isil.veterinaria.veterinaria.service;

import com.isil.veterinaria.veterinaria.model.Vet;
import com.isil.veterinaria.veterinaria.repository.VetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VetService {

    @Autowired
    VetRepository vetRepository;

    @Transactional
    public Vet create(Vet vet){
        return vetRepository.save(vet);
    }

    public List<Vet> findAll(){
        return vetRepository.findAll();
    }
}
