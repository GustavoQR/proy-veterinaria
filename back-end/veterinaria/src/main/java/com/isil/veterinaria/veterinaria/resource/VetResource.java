package com.isil.veterinaria.veterinaria.resource;

import com.isil.veterinaria.veterinaria.model.Vet;
import com.isil.veterinaria.veterinaria.service.VetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class VetResource {

    @Autowired
    private VetService vetService;


    @GetMapping("/vet")
    public ResponseEntity findAll(){
        return new ResponseEntity(vetService.findAll(),
                HttpStatus.OK);
    }

    @PostMapping("/vet")
    public ResponseEntity create(@RequestBody Vet vet){

        vetService.create(vet);

        return new ResponseEntity(vet, HttpStatus.CREATED);
    }
}
